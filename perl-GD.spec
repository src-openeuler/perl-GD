Name:           perl-GD
Version:        2.82
Release:        2
Summary:        A perl5 interface to Thomas Boutell's gd library
License:        GPL-1.0-or-later OR Artistic-2.0
URL:            https://metacpan.org/release/GD
Source0:        https://cpan.metacpan.org/authors/id/R/RU/RURBAN/GD-%{version}.tar.gz

BuildRequires:  coreutils findutils gcc gd-devel make perl-devel perl-ExtUtils-PkgConfig
BuildRequires:  perl-generators perl-interpreter perl(ExtUtils::MakeMaker)

#Test
BuildRequires:  perl(Test::More) perl(lib)
BuildRequires:  perl(Test::NoWarnings) >= 1.00
Requires:       gd

%{?perl_default_filter}

%description
This is a autoloadable interface module for libgd, a popular library
for creating and manipulating PNG files.  With this library you can
create PNG images on the fly or modify existing files.  Features
include:

a.  lines, polygons, rectangles and arcs, both filled and unfilled
b.  flood fills
c.  the use of arbitrary images as brushes and as tiled fill patterns
d.  line styling (dashed lines and the like)
e.  horizontal and vertical text rendering
f.  support for transparency and interlacing
g.  support for TrueType font rendering, via libfreetype.
h.  support for spline curves, via GD::Polyline
i.  support for symbolic font names, such as "helvetica:italic"
j.  support for symbolic color names, such as "green", via GD::Simple
k.  produces output in png, gif, jpeg and xbm format
l.  produces output in svg format via GD::SVG.

%package_help

%prep
%autosetup -n GD-%{version} -p1

perl -pi -e 's|/usr/local/bin/perl\b|%{__perl}|' \
      demos/{*.{pl,cgi},truetype_test}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 OPTIMIZE="%{optflags}"
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%license LICENSE
%doc ChangeLog README README.QUICKDRAW demos/
%{_bindir}/bdf2gdfont.pl
%{perl_vendorarch}/GD/
%{perl_vendorarch}/auto/GD/
%{perl_vendorarch}/GD.pm

%files help
%{_mandir}/man*/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 2.82-2
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Jul 18 2024 xiejing <xiejing@kylinos.cn> - 2.82-1
- update to version 2.82
  - Improve HEIF/AVIF autodetection
  - Fix strawberryperl default libgd path
  - Fix AVIF and Webp autodetection in tests

* Fri Jun 14 2024 xiejing <xiejing@kylinos.cn> - 2.81-1
- update to version 2.81
  - Change GD::Polygon::transform to match old demos (RT #140043),
    and GD::Polyline
  - Add GD::Polygon::rotate(cw-radian) helper
  - Allow GD::Polygon::scale(2.0)

* Mon May 13 2024 xiejing <xiejing@kylinos.cn> - 2.80-1
- update to version 2.80
  - Fix broken copyTranspose and copyReverseTranspose (CPAN RT#153300)
    by Yuriy Yevtukhov
  - Add transformation tests
  - Fix wrong WBMP name and detection
  - Fix wrong filename extension auto-detection for gd,gd2,wbmp
  - Fix wrong filename extension auto-detection for xpm,
    newFromXpm needs the filename, not handle
  - Fix wrong libgd doc link (PR #52) by Tsuyoshi Watanabe

* Sat May 11 2024 xiejing <xiejing@kylinos.cn> - 2.79-1
- update to version 2.79
  - Improve image type autodetection (RT #153212), add a test
  - Fix Avif without Heif config
  - Improve gdlib.pc reader for supported library features

* Sat Oct 07 2023 liweigang <weigangli99@yeah.net> - 2.78-1
- update to version 2.78

* Wed Nov 23 2022 hongjinghao <hongjinghao@huawei.com> - 2.75-2
- modify the package so that it is consistent with Source0

* Mon Oct 24 2022 hongjinghao <hongjinghao@huawei.com> - 2.75-1
- upgrade version to 2.75

* Thu Jan 28 2021 liudabo <liudabo1@huawei.com> - 2.73-1
- upgrade version to 2.73

* Thu Jul 30 2020 wenzhanli<wenzhanli2@huawei.com> - 2.72-1
- Type:NA
- ID:NA
- SUG:NA
- DESC:update version 2.72

* Mon Feb 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.71-1
- Package init
